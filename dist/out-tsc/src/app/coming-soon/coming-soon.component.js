"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var animations_1 = require("@angular/animations");
var ComingSoonComponent = (function () {
    function ComingSoonComponent() {
    }
    ComingSoonComponent.prototype.ngOnInit = function () {
        this.load = true;
    };
    return ComingSoonComponent;
}());
ComingSoonComponent.decorators = [
    { type: core_1.Component, args: [{
                selector: 'app-coming-soon',
                templateUrl: './coming-soon.component.html',
                styleUrls: ['./coming-soon.component.scss'],
                animations: [
                    animations_1.trigger('comingSoon', [
                        animations_1.transition('*=>*', [
                            animations_1.query('span', animations_1.style({ opacity: 0 }), { optional: true }),
                            animations_1.query('span', animations_1.stagger('100ms', [
                                animations_1.animate('1s ease', animations_1.keyframes([
                                    animations_1.style({ opacity: 0, transform: 'translateX(100px)', 'font-weight': 'normal', }),
                                    animations_1.style({ opacity: 1, transform: 'translateX(0px)', 'font-weight': 'bolder' })
                                ]))
                            ]))
                        ])
                    ]),
                    animations_1.trigger('titleSpread', [
                        animations_1.transition('*=>*', [
                            animations_1.query('h1', animations_1.style({ opacity: 0 }), { optional: true }),
                            animations_1.query('h1', animations_1.stagger('100ms', [
                                animations_1.animate('3s ease', animations_1.keyframes([
                                    animations_1.style({ opacity: 0, 'letter-spacing': '-30px', offset: 0 }),
                                    animations_1.style({ opacity: 1, 'letter-spacing': '12px', offset: 1 })
                                ]))
                            ]))
                        ])
                    ])
                ],
            },] },
];
/** @nocollapse */
ComingSoonComponent.ctorParameters = function () { return []; };
exports.ComingSoonComponent = ComingSoonComponent;
//# sourceMappingURL=coming-soon.component.js.map