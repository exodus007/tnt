"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var app_routing_module_1 = require("./app-routing.module");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var app_component_1 = require("./app.component");
exports.AppComponent = app_component_1.AppComponent;
var coming_soon_component_1 = require("./coming-soon/coming-soon.component");
var login_component_1 = require("./login/login.component");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule.decorators = [
    { type: core_1.NgModule, args: [{
                declarations: [
                    app_component_1.AppComponent,
                    coming_soon_component_1.ComingSoonComponent,
                    login_component_1.LoginComponent
                ],
                imports: [
                    platform_browser_1.BrowserModule.withServerTransition({ appId: 'TripNTrade' }),
                    app_routing_module_1.AppRoutingModule,
                    forms_1.FormsModule,
                    http_1.HttpModule,
                    forms_1.ReactiveFormsModule
                ],
                providers: [],
                bootstrap: [app_component_1.AppComponent]
            },] },
];
/** @nocollapse */
AppModule.ctorParameters = function () { return []; };
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map