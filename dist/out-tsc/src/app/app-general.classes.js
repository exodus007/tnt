"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Luser = (function () {
    function Luser() {
        this.identity = '';
        this.pswd = '';
        this.pswd_show = false;
        this.identity_disabled = false;
        this.show_pswd = false;
    }
    return Luser;
}());
exports.Luser = Luser;
//# sourceMappingURL=app-general.classes.js.map