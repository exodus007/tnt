"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var app_general_classes_1 = require("../app-general.classes");
var animations_1 = require("@angular/animations");
var LoginComponent = (function () {
    function LoginComponent(renderer, fb) {
        this.renderer = renderer;
        this.fb = fb;
        this.today = Date.now();
        this.luser = new app_general_classes_1.Luser();
        this.formSubmitting = false;
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.lForm = this.fb.group({
            'identity': ['', forms_1.Validators.compose([forms_1.Validators.required, CustomValidators.pattern(/^[a-z0-9_]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})|[a-z0-9]+$/ig)])],
            'pswd': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(4), forms_1.Validators.maxLength(20)])]
        });
    };
    LoginComponent.prototype.addPost = function () {
        if (this.lForm.controls.identity.invalid) {
            return;
        }
        if (this.lForm.valid) {
            this.formSubmitting = true;
        }
        else {
            this.lForm.controls.identity.disable();
            this.luser.identity_disabled = true;
            this.luser.pswd_show = true;
        }
    };
    LoginComponent.prototype.setEditMode = function () {
        this.luser.identity_disabled = false;
        this.luser.pswd_show = false;
        this.luser.show_pswd = false;
        this.luser.pswd = null;
        this.lForm.controls.identity.enable();
        this.renderer.selectRootElement('#identity').focus();
        this.lForm.controls.pswd.reset();
    };
    LoginComponent.prototype.togglePasswordShow = function () {
        var pswd = this.renderer.selectRootElement('#pswd');
        this.luser.show_pswd = !this.luser.show_pswd;
        if (this.luser.show_pswd) {
            pswd.setAttribute('type', 'text');
        }
        else {
            pswd.setAttribute('type', 'password');
        }
    };
    return LoginComponent;
}());
LoginComponent.decorators = [
    { type: core_1.Component, args: [{
                selector: 'app-login',
                templateUrl: './login.component.html',
                styleUrls: ['./login.component.scss'],
                animations: [
                    animations_1.trigger('pullup', [
                        animations_1.transition('*=>*', [
                            animations_1.query('.pullup', animations_1.style({ opacity: 0 }), { optional: true }),
                            animations_1.query('.pullup', animations_1.stagger('200ms', [
                                animations_1.animate('1s ease', animations_1.keyframes([
                                    animations_1.style({ opacity: 0, transform: 'translateY(100px)', 'font-weight': 'normal', offset: 0 }),
                                    animations_1.style({ opacity: 1, transform: 'translateY(0px)', 'font-weight': 'bolder', offset: 1 })
                                ]))
                            ]))
                        ])
                    ])
                ]
            },] },
];
/** @nocollapse */
LoginComponent.ctorParameters = function () { return [
    { type: core_1.Renderer2, },
    { type: forms_1.FormBuilder, },
]; };
exports.LoginComponent = LoginComponent;
var CustomValidators = (function () {
    function CustomValidators() {
    }
    CustomValidators.pattern = function (reg) {
        return function (control) {
            var value = control.value;
            return value.match(reg) ? null : { 'pattern': { value: value } };
        };
    };
    return CustomValidators;
}());
//# sourceMappingURL=login.component.js.map