"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var login_component_1 = require("./login/login.component");
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var coming_soon_component_1 = require("./coming-soon/coming-soon.component");
var routes = [
    {
        path: '',
        component: coming_soon_component_1.ComingSoonComponent
    },
    {
        path: 'login',
        component: login_component_1.LoginComponent
    }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    return AppRoutingModule;
}());
AppRoutingModule.decorators = [
    { type: core_1.NgModule, args: [{
                imports: [router_1.RouterModule.forRoot(routes)],
                exports: [router_1.RouterModule]
            },] },
];
/** @nocollapse */
AppRoutingModule.ctorParameters = function () { return []; };
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=app-routing.module.js.map