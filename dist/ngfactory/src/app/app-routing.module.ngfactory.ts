/**
 * @fileoverview This file is generated by the Angular template compiler.
 * Do not edit.
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride}
 */
 /* tslint:disable */


import * as i0 from '@angular/core';
import * as i1 from '../../../../src/app/app-routing.module';
import * as i2 from './coming-soon/coming-soon.component.ngfactory';
import * as i3 from './login/login.component.ngfactory';
import * as i4 from '@angular/router';
import * as i5 from '@angular/common';
import * as i6 from '../../../../src/app/coming-soon/coming-soon.component';
import * as i7 from '../../../../src/app/login/login.component';
export const AppRoutingModuleNgFactory:i0.NgModuleFactory<i1.AppRoutingModule> = i0.ɵcmf(i1.AppRoutingModule,
    ([] as any[]),(_l:any) => {
      return i0.ɵmod([i0.ɵmpd(512,i0.ComponentFactoryResolver,i0.ɵCodegenComponentFactoryResolver,
          [[8,[i2.ComingSoonComponentNgFactory,i3.LoginComponentNgFactory]],[3,i0.ComponentFactoryResolver],
              i0.NgModuleRef]),i0.ɵmpd(5120,i4.ActivatedRoute,i4.ɵf,[i4.Router]),i0.ɵmpd(4608,
          i4.NoPreloading,i4.NoPreloading,([] as any[])),i0.ɵmpd(6144,i4.PreloadingStrategy,
          (null as any),[i4.NoPreloading]),i0.ɵmpd(135680,i4.RouterPreloader,i4.RouterPreloader,
          [i4.Router,i0.NgModuleFactoryLoader,i0.Compiler,i0.Injector,i4.PreloadingStrategy]),
          i0.ɵmpd(4608,i4.PreloadAllModules,i4.PreloadAllModules,([] as any[])),i0.ɵmpd(5120,
              i0.NgProbeToken,() => {
                return [i4.ɵb()];
              },([] as any[])),i0.ɵmpd(4608,i4.ɵg,i4.ɵg,[i0.Injector]),i0.ɵmpd(5120,
              i0.APP_INITIALIZER,(p0_0:any) => {
                return [i4.ɵh(p0_0)];
              },[i4.ɵg]),i0.ɵmpd(5120,i4.ROUTER_INITIALIZER,i4.ɵi,[i4.ɵg]),i0.ɵmpd(5120,
              i0.APP_BOOTSTRAP_LISTENER,(p0_0:any) => {
                return [p0_0];
              },[i4.ROUTER_INITIALIZER]),i0.ɵmpd(1024,i4.ɵa,i4.ɵd,[[3,i4.Router]]),
          i0.ɵmpd(512,i4.UrlSerializer,i4.DefaultUrlSerializer,([] as any[])),i0.ɵmpd(512,
              i4.ChildrenOutletContexts,i4.ChildrenOutletContexts,([] as any[])),i0.ɵmpd(256,
              i4.ROUTER_CONFIGURATION,{},([] as any[])),i0.ɵmpd(1024,i5.LocationStrategy,
              i4.ɵc,[i5.PlatformLocation,[2,i5.APP_BASE_HREF],i4.ROUTER_CONFIGURATION]),
          i0.ɵmpd(512,i5.Location,i5.Location,[i5.LocationStrategy]),i0.ɵmpd(512,i0.NgModuleFactoryLoader,
              i0.SystemJsNgModuleLoader,[i0.Compiler,[2,i0.SystemJsNgModuleLoaderConfig]]),
          i0.ɵmpd(1024,i4.ROUTES,() => {
            return [[{path:'',component:i6.ComingSoonComponent},{path:'login',component:i7.LoginComponent}]];
          },([] as any[])),i0.ɵmpd(1024,i4.Router,i4.ɵe,[i0.ApplicationRef,i4.UrlSerializer,
              i4.ChildrenOutletContexts,i5.Location,i0.Injector,i0.NgModuleFactoryLoader,
              i0.Compiler,i4.ROUTES,i4.ROUTER_CONFIGURATION,[2,i4.UrlHandlingStrategy],
              [2,i4.RouteReuseStrategy]]),i0.ɵmpd(512,i4.RouterModule,i4.RouterModule,
              [[2,i4.ɵa],[2,i4.Router]]),i0.ɵmpd(512,i1.AppRoutingModule,i1.AppRoutingModule,
              ([] as any[]))]);
    });
//# sourceMappingURL=data:application/json;base64,eyJmaWxlIjoiL21lZGlhL3Rvbm1veS9Xb3JrL1Nha2EvRGV2ZWxvcG1lbnQvV2ViL0FwcGxpY2F0aW9uL3NyYy9hcHAvYXBwLXJvdXRpbmcubW9kdWxlLm5nZmFjdG9yeS50cyIsInZlcnNpb24iOjMsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm5nOi8vL21lZGlhL3Rvbm1veS9Xb3JrL1Nha2EvRGV2ZWxvcG1lbnQvV2ViL0FwcGxpY2F0aW9uL3NyYy9hcHAvYXBwLXJvdXRpbmcubW9kdWxlLnRzIl0sInNvdXJjZXNDb250ZW50IjpbIiAiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7In0=
