import { NgModule, NgModuleFactory, NgModuleFactoryLoader } from '@angular/core';
import { ServerModule } from '@angular/platform-server';
import { AppModule, AppComponent } from './app.module';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    ServerModule,
    NoopAnimationsModule,
    AppModule
  ],
  bootstrap: [AppComponent]
})
export class AppServerModule { }