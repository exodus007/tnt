export class Luser{
    public identity:string;
    public pswd:string;
    public pswd_show:boolean;
    public identity_disabled:boolean;
    public show_pswd:boolean;
    constructor(){
        this.identity='';
        this.pswd='';
        this.pswd_show=false;
        this.identity_disabled=false;
        this.show_pswd=false;
    }
}