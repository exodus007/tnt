import { Component, OnInit } from '@angular/core';
import { trigger, style, transition, animate, keyframes, query, stagger } from '@angular/animations';
@Component({
  selector: 'app-coming-soon',
  templateUrl: './coming-soon.component.html',
  styleUrls: ['./coming-soon.component.scss'],
  animations: [
    trigger('comingSoon', [
      transition('*=>*', [
        query('span', style({ opacity: 0 }), { optional: true }),
        query('span', stagger('100ms', [
          animate('1s ease', keyframes([
            style({ opacity: 0, transform: 'translateX(100px)', 'font-weight': 'normal', }),
            style({ opacity: 1, transform: 'translateX(0px)', 'font-weight': 'bolder' })
          ]))
        ]))

      ])
    ]),

    trigger('titleSpread', [
      transition('*=>*', [
        query('h1', style({ opacity: 0 }), { optional: true }),
        query('h1', stagger('100ms', [
          animate('3s ease', keyframes([
            style({ opacity: 0, 'letter-spacing': '-30px', offset: 0 }),
            style({ opacity: 1, 'letter-spacing': '12px', offset: 1 })
          ]))
        ]))

      ])
    ])
  ],
})
export class ComingSoonComponent implements OnInit {
  load:boolean;
  constructor() { }

  ngOnInit() {
    this.load=true;
  }

}
