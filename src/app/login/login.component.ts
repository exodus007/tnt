import { Component, OnInit ,Renderer2} from '@angular/core';
import {FormGroup,FormBuilder,Validators,ValidatorFn,AbstractControl} from '@angular/forms';
import {Luser} from '../app-general.classes';
import { trigger, style, transition, animate, keyframes, query, stagger } from '@angular/animations';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [
    trigger('pullup', [
      transition('*=>*', [
        query('.pullup', style({ opacity: 0 }), { optional: true }),
        query('.pullup', stagger('200ms', [
          animate('1s ease', keyframes([
            style({ opacity: 0, transform: 'translateY(100px)', 'font-weight': 'normal',offset:0 }),
            style({ opacity: 1, transform: 'translateY(0px)', 'font-weight': 'bolder',offset:1 })
          ]))
        ]))
      ])
    ])
    ]
})

export class LoginComponent implements OnInit {
  luser:Luser;
  lForm:FormGroup;
  formSubmitting:boolean;
  today: number = Date.now();
  constructor(private renderer: Renderer2,private fb:FormBuilder){
    this.luser=new Luser();
    this.formSubmitting=false;
    
  }
  ngOnInit() {
    this.lForm=this.fb.group({
      'identity':['',Validators.compose([Validators.required,CustomValidators.pattern(/^[a-z0-9_]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})|[a-z0-9]+$/ig)])],
      'pswd':['',Validators.compose([Validators.required,Validators.minLength(4),Validators.maxLength(20)])]
    })
  }
  addPost() {
    
    if(this.lForm.controls.identity.invalid){
      return;
    }
    if(this.lForm.valid){
      this.formSubmitting=true;
    }else{
      this.lForm.controls.identity.disable();
      this.luser.identity_disabled=true;
      this.luser.pswd_show=true;
    }
  }
  setEditMode(){
    this.luser.identity_disabled=false;
    this.luser.pswd_show=false;
    this.luser.show_pswd=false;
    this.luser.pswd=null;
    this.lForm.controls.identity.enable();
    this.renderer.selectRootElement('#identity').focus();
    this.lForm.controls.pswd.reset();
  }
  togglePasswordShow(){
    let pswd=this.renderer.selectRootElement('#pswd');
    this.luser.show_pswd=!this.luser.show_pswd;
    if(this.luser.show_pswd){
      pswd.setAttribute('type','text');
    }else{
      pswd.setAttribute('type','password');
    }
  }
}

class CustomValidators {

    public static pattern(reg: RegExp) : ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            var value = <string>control.value;
            return value.match(reg) ? null : { 'pattern': { value } };
        }
    }
}
